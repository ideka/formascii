from widgets import *


class LineFrame(Frame):
    def __init__(self, left, right):
        super().__init__()

        self.horizontal = True
        self.content = [
            Label().set(content='{0}: '.format(left)),
            Text().set(content=right)
        ]


class DayFrame(Frame):
    def __init__(self, month, day, weekday, *args):
        super().__init__()

        self.bottom_padding = 1
        self.content = [
            Frame().set(horizontal=True,
                        content=[
                            Art().set(content='[  ]~~'),
                            Label().set(content='{0}.{1}.{2}'.
                                        format(month, day, weekday)),
                            Line().set(pattern='~')
                        ])
        ] + list(args)


class SuggestedLinkFrame(Frame):
    def __init__(self, link, answers, pointsw, pointswo, pointsnext):
        super().__init__()

        self.content = [
            Frame().set(horizontal=True,
                        content=[
                            Label().set(content='Suggested link: '),
                            Text().set(content=link)
                        ]),
            Frame().set(left_padding=1,
                        content=[
                            LineFrame('Answer', answers),
                            LineFrame('Pts w/ (w/o) matching persona',
                                      '{0} ({1})'.format(pointsw, pointswo)),
                            LineFrame('Pts for next rank', pointsnext)
                        ])
        ]


class SomeText(TextAroundFrame):
    def __init__(self, texta, textb):
        super().__init__()

        self.frame_align = FrameAlign.right
        self.frame = SimpleFrame().\
                set(left_padding=1,
                    right_padding=1,
                    content=[
                        Text().set(content=texta,
                                   width=20)
                    ])


        self.content = textb
        self.align = TextAlign.right

class Document(DocumentFrame):
    def __init__(self):
        super().__init__()

        self.width = 79
        self.content = [
            DayFrame('09', '15', 'TUE',
                     SuggestedLinkFrame('Magician 9', 'Confrount Emiri',
                                        '22', '15', '40'),
                     LineFrame('Night event', 'Walk: Shinjiro')),
            SomeText('Some Label\nother label\n and another one', """Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.""")
        ]
