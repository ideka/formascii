from widget import Widget


class Line(Widget):
    """Widget representing a straight horizontal line."""

    def __init__(self):
        super().__init__()

        # Pattern of the line.
        self.pattern = '-'

    def get_width(self):
        return 0

    def render(self, canvas):
        for x in range(canvas.width):
            canvas.put_char(x, 0, self.pattern[x % len(self.pattern)])
