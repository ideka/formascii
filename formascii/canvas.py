class Canvas:
    """Canvas to draw widgets to. Has a fixed width and a flexible height.

    The height starts at 0 but gets bigger as the canvas is drawn to.

    """

    def __init__(self, callback, width):
        """Create a canvas with the given callback and width.

        The callback is a function to be called every time the canvas is
        drawn to.

        """
        assert(width > 0)
        self.callback = callback
        self.width = width
        self.height = 0

    def subcanvas(self, x, y, width=0):
        """Return a subcanvas from the given position.

        All things drawn to the subcanvas will be also drawn to the canvas at
        the appropiate position.

        If the given width is 0, use the same width as the canvas.

        """
        def callback(xx, yy, char):
            self.put_char(x + xx, y + yy, char)
        assert(x >= 0)
        assert(y >= 0)
        if width == 0:
            width = self.width
        assert(x + width <= self.width)
        return Canvas(callback, width)

    def put_char(self, x, y, char):
        """Put the given character at the given position on the canvas.

        Update the canvas' height accordingly.

        """
        if x >= 0 and x < self.width:
            self.callback(x, y, char)
            self.height = max(self.height, y + 1)
