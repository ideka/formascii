class Widget:
    """A widget. It has a width, and can be rendered to a canvas."""

    def set(self, **kwargs):
        """Set all given keyword arguments as variables of the widget."""

        self.__dict__.update(kwargs)
        return self

    def get_width(self):
        """Return the width of the widget or 0 if the width is flexible."""

        raise NotImplementedError()

    def render(self, canvas):
        """Render the widget to the given canvas."""

        raise NotImplementedError()
