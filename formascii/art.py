from widget import Widget

class Art(Widget):
    """Widget for static fixed-width multiline content."""

    def __init__(self):
        # Content of the art widget.
        content = ''

    def get_width(self):
        return max(len(line) for line in self.content.split('\n'))

    def render(self, canvas):
        assert(canvas.width >= self.get_width())

        for y, line in enumerate(self.content.strip('\n').split('\n')):
            for x, char in enumerate(line):
                canvas.put_char(x, y, char)
