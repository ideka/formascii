import enum
from widget import Widget

WORD_BREAKS = ' '


@enum.unique
class TextAlign(enum.Enum):
    left = 0
    center = 1
    right = 2


class AbstractText(Widget):
    """Abstract Widget for aligned text. Has no defined width."""

    def __init__(self):
        super().__init__()

        # Content of the text.
        self.content = ''
        # Alignment of the text.
        self.align = TextAlign.left

    def render(self, canvas):
        self.render_lines(canvas, self.get_lines(canvas.width,
                                                 self.content)[0])

    def get_lines(self, width, content, maxlines=0):
        """Break the given content into lines of the given width.

        If maxlines is not 0, don't produce any more lines than that.

        Return a tuple containing a list of lines and any leftover content.

        """
        lines = []
        content = content.strip()
        while len(content) > 0 and (maxlines == 0 or len(lines) < maxlines):
            content = content.strip()

            # Look for a line feed.
            i = content.find('\n')
            # If there's one before the line ends...
            if i != -1 and i < width:
                # Add up to it to a line.
                lines.append(content[:i])
                # Remove the line from the content.
                content = content[i + 1:]
                # And go again.
                continue

            # If the rest of the content fits in a line...
            if len(content) < width:
                # Add the whole content to a line.
                lines.append(content)
                # Empty the content.
                content = ''
                # And leave.
                break

            # Go backwards from the first character that doesn't fit the line.
            for i, char in enumerate(content[width::-1]):
                # If we find a character considered a word break...
                if char in WORD_BREAKS:
                    # Add up to it to a line.
                    lines.append(content[:width - i])
                    # Remove the line from the content.
                    content = content[width - i:]
                    # And leave this inner loop.
                    break
            else:
                # If we went all the way back and found no word break
                # character, just add all to a line.
                lines.append(content[:width])
                # And remove the line from the content.
                content = content[width:]
        return lines, content

    def render_lines(self, canvas, lines):
        """Render each line into the given canvas with the right alignment."""
        for y, line in enumerate(lines):
            padding = canvas.width - len(line)
            if self.align == TextAlign.left:
                padding = 0
            if self.align == TextAlign.center:
                padding //= 2
            for x, char in enumerate(line):
                canvas.put_char(x + padding, y, char)


class Label(AbstractText):
    """AbstractText for single-line fixed-width text."""

    def get_width(self):
        return len(self.content)

    def render(self, canvas):
        self.render_lines(canvas, [self.content.strip()])


class Text(AbstractText):
    """AbstractText with a set width."""

    def __init__(self):
        super().__init__()

        # Width of the text. 0 for flexible width.
        self.width = 0

    def get_width(self):
        return self.width


@enum.unique
class FrameAlign(enum.Enum):
    left = 0
    right = 1


class TextAroundFrame(Text):
    """Text to be drawn around a Frame."""

    def __init__(self):
        super().__init__()

        # Frame.
        self.frame = None
        # Alignment of the Frame on the text.
        self.frame_align = FrameAlign.left
        # Horizontal separation between the Frame and the text.
        self.h_sep = 2
        # Vertical separation between the Frame and the text.
        self.v_sep = 1

    def render(self, canvas):
        if self.frame is None:
            return super().render(canvas)

        frame_width = self.frame.get_width()
        text_width = canvas.width - frame_width

        frame_canvas = canvas.subcanvas(0
                                        if self.frame_align == FrameAlign.left
                                        else text_width,
                                        0, frame_width)
        self.frame.render(frame_canvas)

        text_canvas = canvas.subcanvas(0
                                       if self.frame_align == FrameAlign.right
                                       else frame_width + self.h_sep,
                                       0, text_width - self.h_sep)
        lines, content = self.get_lines(text_canvas.width, self.content,
                                        frame_canvas.height + self.v_sep)
        self.render_lines(text_canvas, lines)
        self.render_lines(canvas.subcanvas(0, text_canvas.height),
                          self.get_lines(canvas.width, content)[0])
