#!/usr/bin/python3

import sys


def main():
    if len(sys.argv) < 2:
        print('No module given.')
        return 1

    document = __import__(sys.argv[1]).Document()
    result = []

    def callback(x, y, char):
        if char == '':
            return
        while len(result) <= y:
            result.append([' '] * document.width)
        result[y][x] = char

    document.render_to_callback(callback)

    for line in result:
        for char in line:
            print(char, end='')
        print('')

    return 0

if __name__ == '__main__':
    sys.exit(main())
