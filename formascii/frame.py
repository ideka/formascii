from canvas import Canvas
from widget import Widget


class Frame(Widget):
    """Widget that contains other widgets horizontally or vertically.

    Frames might also have padding or be decorated with single characters on
    their sides or corners.

    """

    def __init__(self):
        super().__init__()

        # Frame's content.
        self.content = []
        # Whether the content should be arranged horizontally or vertically.
        self.horizontal = False
        # Separation between each item in the frame.
        self.separation = 0

        # Decorations. One character max.
        self.left = ''
        self.right = ''
        self.top = ''
        self.bottom = ''
        self.top_left = ''
        self.top_right = ''
        self.bottom_left = ''
        self.bottom_right = ''

        # Padding of the content of the frame.
        self.left_padding = 0
        self.right_padding = 0
        self.top_padding = 0
        self.bottom_padding = 0

    def get_width(self):
        maxwidth = 0
        for item in self.content:
            width = item.get_width()
            if width == 0:
                return 0
            maxwidth = max(maxwidth, width)
        return (maxwidth + len(self.left) + len(self.right) +
                self.left_padding + self.right_padding)

    def render(self, canvas):
        assert(len(self.left) <= 1)
        assert(len(self.right) <= 1)
        assert(len(self.top) <= 1)
        assert(len(self.bottom) <= 1)
        assert(len(self.top_left) <= 1)
        assert(len(self.top_right) <= 1)
        assert(len(self.bottom_left) <= 1)
        assert(len(self.bottom_right) <= 1)

        subcanvas = canvas.subcanvas(self.left_padding + len(self.left),
                                     self.top_padding + len(self.top),
                                     canvas.width - self.left_padding -
                                     self.right_padding - len(self.left) -
                                     len(self.right))

        if self.horizontal:
            remainder = subcanvas.width
            flexible_width_items = 0
            for item in self.content:
                w = item.get_width()
                if w != 0:
                    remainder -= w
                else:
                    flexible_width_items += 1
                remainder -= self.separation
            remainder += self.separation
            flexible_width = 0
            if flexible_width_items > 0:
                flexible_width = remainder // flexible_width_items
            surplus = flexible_width * flexible_width_items < remainder
            x = 0
            for item in self.content:
                w = item.get_width()
                if w == 0:
                    w = flexible_width + surplus
                    surplus = 0
                subsubcanvas = subcanvas.subcanvas(x, 0, w)
                item.render(subsubcanvas)
                x += w + self.separation
        else:
            y = 0
            for item in self.content:
                subsubcanvas = subcanvas.subcanvas(0, y, item.get_width())
                item.render(subsubcanvas)
                y += subsubcanvas.height + self.separation

        canvas.put_char(0, canvas.height - 1 +
                        self.bottom_padding + len(self.bottom), '')

        if self.top_left != '':
            canvas.put_char(0, 0, self.top_left)
        if self.top_right != '':
            canvas.put_char(canvas.width - 1, 0, self.top_right)
        if self.bottom_left != '':
            canvas.put_char(0, canvas.height - 1, self.bottom_left)
        if self.bottom_right != '':
            canvas.put_char(canvas.width - 1, canvas.height - 1,
                            self.bottom_right)
        for x in range(canvas.width - 2):
            if self.top != '':
                canvas.put_char(x + 1, 0, self.top)
            if self.bottom != '':
                canvas.put_char(x + 1, canvas.height - 1, self.bottom)
        for y in range(canvas.height - 2):
            if self.left != '':
                canvas.put_char(0, y + 1, self.left)
            if self.right != '':
                canvas.put_char(canvas.width - 1, y + 1, self.right)


class SimpleFrame(Frame):
    """Frame with a default simple decoration."""

    def __init__(self):
        super().__init__()

        self.left = '|'
        self.right = '|'
        self.top = '-'
        self.bottom = '-'
        self.top_left = '.'
        self.top_right = '.'
        self.bottom_left = "'"
        self.bottom_right = "'"


class DocumentFrame(Frame):
    """Frame representing the main document. Has a fixed width."""

    def __init__(self):
        super().__init__()

        # Width of the document.
        self.width = 80

    def get_width(self):
        return self.width

    def render_to_callback(self, callback):
        """Render to a canvas with the given callback.

        Use the document's width.

        """
        self.render(Canvas(callback, self.get_width()))
